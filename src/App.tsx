import React, { useState, useEffect } from "react";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin from "@fullcalendar/interaction";
import timeGridPlugin from "@fullcalendar/timegrid";
import http from "./service/api";
import { IBook } from "./model/IBook";

import "./App.css";
import NewDatesItem from "./NewDatesItem";
import moment from "moment";

interface CalenderDate {
  start: Date;
  end: Date;
  overlape: boolean;
}

const App: React.FC = () => {
  const [dates, setDates] = useState<CalenderDate[]>([]);
  const [newDates, setNewDates] = useState<CalenderDate[]>([]);

  const handleRemove = (i: number) => {
    const allNewDates = newDates.filter((_date, index) => i !== index);
    setNewDates(allNewDates);
  };

  useEffect(() => {
    http.fetch().then((response: IBook[]) => {
      const updatedDates: CalenderDate[] = [];
      response.forEach((date: IBook) => {
        updatedDates.push({
          start: new Date(date.start_at),
          end: new Date(date.end_at),
          overlape: false
        });
      });
      setDates(updatedDates);
    });
  }, []);

  const handleDateSelect = ({ start, end }: { start: Date; end: Date }) => {
    if (moment(start).isBefore(new Date())) {
      return false;
    }
    setNewDates([...newDates, { start: start, end: end, overlape: false }]);
  };

  const handleResize = (info: any) => {
    const updatedDates = newDates.filter(
      date =>
        !info.event.start ||
        !moment(date.start).isSame(new Date(info.event.start))
    );

    const isOverLaping = updatedDates.findIndex(date => {
      return (
        info.event.start &&
        info.event.end &&
        moment(date.start).isBetween(
          new Date(info.event.start),
          new Date(info.event.end)
        )
      );
    });

    if (isOverLaping === -1) {
      if (info.event.start && info.event.end) {
        updatedDates.push({
          start: info.event.start,
          end: info.event.end,
          overlape: false
        });
        setNewDates([...updatedDates]);
      }
    }
  };

  return (
    <div className="app">
      <div className="calender-section">
        <FullCalendar
          selectable={true}
          defaultView="timeGridWeek"
          plugins={[dayGridPlugin, interactionPlugin, timeGridPlugin]}
          selectOverlap={false}
          select={handleDateSelect}
          events={[...dates, ...newDates]}
          eventBackgroundColor="green"
          editable={true}
          slotEventOverlap={false}
          eventStartEditable={false}
          eventResize={handleResize}
          header={{
            left: "prev,next today",
            center: "title",
            right: "dayGrid12Days,timeGridWeek,timeGridDay"
          }}
          views={{
            dayGrid12Days: {
              type: "timeGridDay",
              duration: { days: 15 },
              buttonText: "15 day"
            }
          }}
        />
      </div>
      <div className="sidebar">
        <div className="sidebar__content">
          {newDates.map((newDate: CalenderDate, i) => {
            return (
              <NewDatesItem
                key={i}
                handleRemove={() => handleRemove(i)}
                date={newDate}
              />
            );
          })}
          {newDates.length !== 0 ? (
            <div className="action__button">
              <i
                onClick={() => {
                  const allDates = dates;
                  allDates.push(...newDates);
                  setDates(allDates);
                  const apiDates = newDates.map(date => {
                    return { start_at: date.start, end_at: date.end };
                  });
                  http.add(apiDates).then(() => setNewDates([]));
                }}
                className="fa icon success fa-plus"
              ></i>
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default App;
