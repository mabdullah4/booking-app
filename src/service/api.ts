import axios from "axios";

const instance = axios.create({
  baseURL: "https://e2workci.ddns.net/apitest/"
});

const bookingApi = {
  fetch: () => instance.get("calendar.php").then(response => response.data),
  add: (data: { start_at: Date; end_at: Date }[]) => {
    return instance.post("booking.php", data);
  }
};

export default bookingApi;
