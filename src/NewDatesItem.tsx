import * as React from "react";
import moment from "moment";

export interface NewDatesItemProps {
  date: { start: Date; end: Date };
  handleRemove: Function;
}

const NewDatesItem: React.SFC<NewDatesItemProps> = ({
  date,
  handleRemove
}) => {
  return (
    <div className="date__item">
      <div className="start__date">
        <span className="day">{moment(date.start).format("ddd")}</span>
        <span className="time">{moment(date.start).format("hh/mm a")}</span>
        <span className="date">{moment(date.start).format("DD/MM")}</span>
      </div>
      <div className="end__date">
        <span className="day">{moment(date.end).format("ddd")}</span>
        <span className="time">{moment(date.end).format("hh/mm a")}</span>
        <span className="date">{moment(date.end).format("DD/MM")}</span>
      </div>
      <div className="actions">
        <i
          onClick={() => handleRemove(date)}
          className="fa icon danger fa-trash"
        ></i>
      </div>
    </div>
  );
};

export default NewDatesItem;
