import { IVehicle } from "./IVehicle";
import { IUser } from "./IUser";

export interface IBook {
  id: number;
  user: IUser;
  vehicle: IVehicle;
  start_at: Date;
  end_at: Date;
}